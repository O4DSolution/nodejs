/*
 * Author : Shakti Kumar
 * Demostration how to insert and select data in node js.
 * This program demostrate below service:
 * 1. When server start and user hit the project context path, it return index.html pages.
     ex: http://localhost:8081/
   2. When user hit http://localhost:8081/save, it store the request parameter into
      mysql database.
   3. http://localhost:8081/showAll, it display list of all data store in database in json format.

 */
var express = require('express');
var app = express();

var mysql = require('mysql');
var connection = mysql.createConnection({
  host     : '127.0.0.1', // Change host name
  user     : 'root',      // Username of databse
  password : '',          // Password if any
  database : 'test'       // Name of Database where user want to store databse.
});


var bodyParser = require('body-parser');
var urlencodedParser = bodyParser.urlencoded({ extended: false })
var express = require("express");
var app     = express();
var path    = require("path");
app.set('view engine', 'ejs');

app.get('/', function (req, res) {
    // connected! (unless `err` is set)
	//connection.connect();
	// When context root of project is traversed, it should land ther user to index.html page.
      res.sendFile(path.join(__dirname+'/index.html'));
})

app.post('/save',urlencodedParser, function(req, res){

	console.log( "In save function hit...");

   var name = req.body.vreg;
   var vmodel = req.body.vmodel;
   var chkbox = req.body.ckbox;
   var category=req.body.Category;
   var workfor = req.body.workfor;
   var intime = req.body.intime;
   var outtime = req.body.outtime;


// only for debug purpose, remove it later
	 console.log( name);
	 console.log( vmodel);
	 console.log( chkbox);
	 console.log( category);
	 console.log( workfor);
	 console.log( intime);
	 console.log( outtime);

	console.log("Connection Successfull with database");

    // SQL INSERT Statment, modify as per data inserted.
    var sql = "INSERT INTO test (vname,vmodel,category,workfor,intime,outtime) values ?";
	var values=[  [name, vmodel,category,workfor,intime,outtime] ];

	//Call back function to insert data into DB
	connection.query(sql, [values], function (error, results, fields) {
	  if (error) throw error;
	  console.log(results.insertId);
	});

  //After that redirect the page to particular HTML page.
  res.sendFile(path.join(__dirname+'/index.html'));
});


app.get('/showAll', function(req, res){
	console.log( "In show function hit...");

	connection.query('SELECT * from test order by 1 desc', function (error, results, fields) {
		  if (error) throw error;
		  console.log('The solution is: ', results);
	      res.render('display.ejs',{page_title:"Test Table",data:results});
  		//res.json(results);
	});

});


// Below is listen port of app, change as per user requirement.
var server = app.listen(8081, function () {

  var host = server.address().address
  var port = server.address().port
  console.log("Example app listening at http://%s:%s", host, port)

})









